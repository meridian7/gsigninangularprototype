// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCJsgFIJBo2J69qowI6xK8rPd-oxc5mcoI",
    authDomain: "myproj-f16b9.firebaseapp.com",
    databaseURL: "https://myproj-f16b9-default-rtdb.firebaseio.com",
    projectId: "myproj-f16b9",
    storageBucket: "myproj-f16b9.appspot.com",
    messagingSenderId: "436897515765",
    appId: "1:436897515765:web:7f104e88ada7f533cd1dbc",
    measurementId: "G-J05SP5NRD3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
