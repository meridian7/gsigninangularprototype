import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList }from '@angular/fire/database';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  todos$: AngularFireList<any[]>;
  constructor(private af: AngularFireDatabase) {}
  ngOnInit() {
    this.todos$ = this.af.list('/todos');
  }
  addTodo(value: string): void {
    // ...
  }
  deleteTodo(todo: any): void {
    // ...
  }
  toggleDone(todo: any): void {
    // ...
  }
}
